import telegram
from pymongo import MongoClient
from bson import SON

import callback
import mongoConnector
from telegram import (InlineKeyboardButton,InlineKeyboardMarkup)
import time

def start(bot, update):
    texto="Bienvenido al bot de gasolineras de España!! Utilízame para ver la gasolina más barata en tu localidad o provincia o para encontrar la gasolinera más cercana a tu ubicación. \n Para conocer los comandos disponibles escribe /help"
    bot.send_message(chat_id=update.message.chat_id, text=texto)


def help(bot, update):
    texto="¿Qué quieres hacer? \n-/gasolinabaratalocalidad nombre_localidad :para encontrar la gasolina más barata de tu localidad\n-/gasolinabarataprovincia nombre_provincia :para encontrar la gasolina más barata de tu provincia \n-/mascercana : para encontrar la gasolinera más barata a 10km de su posicion."
    bot.send_message(chat_id=update.message.chat_id, text=texto)

def mascercana(bot, update):
    location_keyboard = telegram.KeyboardButton(text="Enviar Ubicacion", request_location=True)
    custom_keyboard = [[location_keyboard]]
    reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard, resize_keyboard=True)
    bot.send_message(chat_id=update.message.chat_id, text="Enviame tu ubicacion!", reply_markup=reply_markup)


    # kb = [[telegram.KeyboardButton('/local', request_location=True)]]
    # kb_markup = telegram.ReplyKeyboardMarkup(kb, resize_keyboard=True)
    # bot.send_message(chat_id=update.message.chat_id, text="your message ", reply_markup=kb_markup)


def gasolinabarataprovincia(bot, update, args):
    print(update.message.from_user)
    provincia = ' '.join(args).upper()
    keyboard = [[InlineKeyboardButton("Sin Plomo 95", callback_data='provincia#'+provincia+'#precio_gasolina_95'),
                 InlineKeyboardButton("Diesel", callback_data='provincia#'+provincia+'#precio_gasóleo_a')]]
    reply_markup = InlineKeyboardMarkup(keyboard)

    d = update.message.reply_text('Elige el tipo de combustible:', reply_markup=reply_markup)
    print(d)
    bot.send_message

def gasolinabaratalocalidad(bot, update, args):
    print(update.message.from_user)
    localidad = ' '.join(args).upper()

    keyboard = [[InlineKeyboardButton("Sin Plomo 95", callback_data='localidad#' + localidad + '#precio_gasolina_95'),
                 InlineKeyboardButton("Diesel", callback_data='localidad#' + localidad + '#precio_gasóleo_a')]]
    reply_markup = InlineKeyboardMarkup(keyboard)
    update.message.reply_text('Elige el tipo de combustible: ', reply_markup=reply_markup)

