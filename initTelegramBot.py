import telegram
from telegram.ext import (Updater, CommandHandler, Filters, CallbackQueryHandler, MessageHandler)
from pymongo import MongoClient
import commands
import constants
import logging
import callback
import mongoConnector

def main():
    t_bot = telegram.Bot(token=constants.BOT_TOKEN)
    updater = Updater(token=constants.BOT_TOKEN)
    dispatcher = updater.dispatcher
    logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)
    # commands handlers
    start_handler = CommandHandler('start', commands.start)
    help_handler = CommandHandler('help', commands.help)
    mascercana_handler = CommandHandler('mascercana', commands.mascercana)
    cheaper_fuel_province_handler = CommandHandler('gasolinaBarataProvincia', commands.gasolinabarataprovincia, pass_args=True)
    cheaper_fuel_city_handler = CommandHandler('gasolinaBarataLocalidad', commands.gasolinabaratalocalidad, pass_args=True)
    # messages handlers
    callback_query_handler = CallbackQueryHandler(callback.button)
    dispatcher.add_handler(mascercana_handler)
    dispatcher.add_handler(start_handler)
    dispatcher.add_handler(help_handler)
    dispatcher.add_handler(cheaper_fuel_city_handler)
    dispatcher.add_handler(cheaper_fuel_province_handler)
    dispatcher.add_handler(callback_query_handler)
    updater.start_polling()
    dispatcher.add_handler(MessageHandler(Filters.location, callback.start_callback))


if __name__ == '__main__':
    main()
