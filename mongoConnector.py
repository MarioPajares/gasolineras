import json
import pymongo
from bson import SON
from pymongo import MongoClient
import requests
from normalice import normalizar_nombres_lugares

def download_data():
    return requests.request("GET", "https://opendata.arcgis.com/datasets/e64c741c13464d418f66bf3a5badeda2_0.geojson")


def insert_data(data, collection):
    try:
        document = json.loads(data.text)
        for i in document["features"]:
            coordinates = [i["properties"]["longitud"], i["properties"]["latitud"]]
            i["properties"]["geocoordinates"] = coordinates
            i["properties"]["municipio"] = normalizar_nombres_lugares(str(i["properties"]["municipio"]))
            i["properties"]["provincia"] = normalizar_nombres_lugares(str(i["properties"]["provincia"]))
            # print(i["properties"]["provincia"])
            collection.insert_one(i["properties"])
        print("Todos los datos insertados")
    except:
        print("error en la inserccion")


def update_data(db, collection):
    data = download_data()
    db.collection.drop()
    insert_data(data, collection)


def consult_by_province(province, collection):
    return collection.find({"provincia": province})


def get_cheapest_fuel_by_province(province, kin_of_fuel):
    client = MongoClient()
    client = MongoClient('localhost', 27017)
    db = client['gasolinerasdb']
    collection = db['coleccionGasolineras']
    resultado = collection.find({"provincia": province}).sort([(kin_of_fuel, pymongo.ASCENDING)])
    for i in resultado:
        if (i[kin_of_fuel]) is not None:
            return i[kin_of_fuel], i["rótulo"], i["dirección"], i["municipio"], i["latitud"], i["longitud"]
            breaks


def get_cheapest_fuel_by_town(town, kin_of_fuel):
    client = MongoClient()
    client = MongoClient('localhost', 27017)
    db = client['gasolinerasdb']
    collection = db['coleccionGasolineras']
    resultado = collection.find({"municipio": town}).sort([(kin_of_fuel, pymongo.ASCENDING)])
    for i in resultado:
        if (i[kin_of_fuel]) is not None:
            return i[kin_of_fuel], i["rótulo"], i["dirección"], i["latitud"], i["longitud"]
            breaks


def get_cheapest_by_location(lon_origen, lat_origen, kin_of_fuel):
    # print(lat_origen, lon_origen)
    client = MongoClient('localhost', 27017)
    db = client['gasolinerasdb']
    collection = db['coleccionGasolineras']
    maxDistance = 10000
    lat_origen = float(lat_origen)
    lon_origen = float(lon_origen)
    query = {'geocoordinates': {'$near': SON([('$geometry', SON([('type', 'Point'), ('coordinates', [lat_origen, lon_origen])])),('$maxDistance', maxDistance)])}}
    doc = collection.find(query).sort([(kin_of_fuel, pymongo.ASCENDING)])[0]
    lat = doc["latitud"]
    lon = doc["longitud"]
    url = "https://www.google.es/maps/place/" + str(lat) + "," + str(lon)
    return url
