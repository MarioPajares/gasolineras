import json

import requests
def getDistance(latitude, longitude, latitudedest, longitudedest):
    resultado = requests.request("GET","https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins="+str(latitude)+","+str(longitude)+"&destinations="+str(latitudedest)+","+str(longitudedest)+"&key=AIzaSyDTMrEAEVHA3iLjEe08zc7VY4qATcKZDq0")
    return (json.loads(resultado.text)["rows"][0]["elements"][0]["duration"]["value"])/60
# latitude = 39.490444
# longitude = -6.367694
# latitudedest = 39.576371
# longitudedest = -6.263831
# print(destination.getDistance(latitude, longitude, latitudedest, longitudedest))
