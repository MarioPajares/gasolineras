from pymongo import MongoClient
import mongoConnector

def initDB():
    client = MongoClient('localhost', 27017)
    db = client['gasolinerasdb']
    collection = db['coleccionGasolineras']
    collection.create_index([("geocoordinates","2dsphere")])
    return db, collection


def main():
    print("Comienza la actualizacion de los datos")
    db, collection = initDB()
    mongoConnector.update_data(db, collection)

if __name__ == '__main__':
    main()