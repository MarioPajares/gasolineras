import telegram
from bson import SON
from pymongo import MongoClient
from telegram import InlineKeyboardButton, InlineKeyboardMarkup, KeyboardButton

import commands
import mongoConnector
from normalice import normalizar_nombres_lugares


def button(bot, update):

    query = update.callback_query
    print(str(query.data))
    tipo_consulta = str(query.data).split("#")[0]

    if tipo_consulta == "mascercana":
        kind_fuel = str(query.data).split("#")[1]
        lat = str(query.data).split("#")[2]
        lon = str(query.data).split("#")[3]
        print(kind_fuel)
        print(lat)
        print(lon)
        url = mongoConnector.get_cheapest_by_location(lon, lat, kind_fuel)
        print(url)
        # update.message.reply_text(url)
        # bot.edit_message_text(chat_id=update.message.chat_id, message_id=update.message.message_id, text=url)
        bot.edit_message_text(chat_id=query.message.chat_id, message_id=query.message.message_id, text=url)
    else:
        lugar = normalizar_nombres_lugares(str(query.data).split("#")[1])
        combustible = str(query.data).split("#")[2]
        print(tipo_consulta, lugar, combustible)
        if combustible == 'precio_gasolina_95':
            escritura_gasolina = "La gasolina sin plomo 95 mas barata de "
        else:
            escritura_gasolina = "El diesel más barato de "
        if tipo_consulta == "provincia":
            try:
                precio, nombre, direccion, municipio, lat, lon = mongoConnector.get_cheapest_fuel_by_province(lugar, combustible)
                url = "https://www.google.es/maps/place/" + str(lat) + "," + str(lon)
                message = escritura_gasolina + lugar + ' es ' + nombre + '\nSu precio es de ' + str(
                    precio).replace(".", ",") + '€.\nSe encuentra en la localidad de ' + municipio + '\n\n' + url
            except:
                message = " El lugar introducido no es valido"
        else:
            try:
                print(lugar, combustible)
                precio, nombre, direccion, lat, lon = mongoConnector.get_cheapest_fuel_by_town(lugar, combustible)
                url = "https://www.google.es/maps/place/" + str(lat) + "," + str(lon)
                message = escritura_gasolina + str(lugar) + ' es ' + nombre + '\nSu precio es de ' + str(precio).replace(".",",") + '€ \nSe encuentra en la calle: ' + direccion + '\n' + url
            except:
                message = " El lugar introducido no es valido"
        bot.edit_message_text(chat_id=query.message.chat_id, message_id=query.message.message_id,  text=message)


def start_callback(bot, update):
    lat = update.message.location.longitude
    lon = update.message.location.latitude
    keyboard = [[InlineKeyboardButton("Sin Plomo 95", callback_data="mascercana#precio_gasolina_95#"+str(lat)+"#"+str(lon)+""),
                 InlineKeyboardButton("Diesel", callback_data="mascercana#precio_gasóleo_a#"+str(lat)+"#"+str(lon)+"")]]

    reply_markup = InlineKeyboardMarkup(keyboard, resize_keyboard=True)
    update.message.reply_text('Elige el tipo de combustible: ', reply_markup=reply_markup)
    # bot.send_message(chat_id=update.message.chat_id, text=url)
#
# def start_callback(bot, update):
#     client = MongoClient('localhost', 27017)
#     db = client['gasolinerasdb']
#     collection = db['coleccionGasolineras']
#     maxDistance = 50000
#     query = {'lat': {'$near': SON([('$geometry', SON([('type', 'Point'), ('coordinates', [update.message.location.longitude, update.message.location.latitude])])),('$maxDistance', maxDistance)])}}
#     doc = collection.find(query).sort("")
#     lat = doc["latitud"]
#     lon = doc["longitud"]
#     url = "https://www.google.es/maps/place/" + str(lat) + "," + str(lon)
#
#
#
#     bot.send_message(chat_id=update.message.chat_id, text=url)
